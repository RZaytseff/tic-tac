create schema TicTac;

SET CURRENT SCHEMA TicTac;

--DROP TABLE Gamer;
CREATE TABLE Gamer ( 
	id  int NOT NULL GENERATED ALWAYS AS IDENTITY 
		(START WITH 1, INCREMENT BY 1),
	nic      CHAR(20) NOT NULL,
	login    CHAR(20) NOT NULL,
	password CHAR(20) NOT NULL
);

ALTER TABLE Gamer ADD CONSTRAINT PK_Gamer_id PRIMARY KEY (id);
-- ALTER TABLE Gamer ADD CONSTRAINT UQ_Gamer_id UNIQUE (id);
ALTER TABLE Gamer ADD CONSTRAINT UQ_nic UNIQUE (nic);
ALTER TABLE Gamer ADD CONSTRAINT UQ_login UNIQUE (login);

--------------------------------------------------------------------
--DROP TABLE State;
CREATE TABLE State ( 
	id  int NOT NULL GENERATED ALWAYS AS IDENTITY 
		(START WITH 1, INCREMENT BY 1),
	state CHAR(20) NOT NULL
);

ALTER TABLE State ADD CONSTRAINT PK_State_id PRIMARY KEY (id);
-- ALTER TABLE UserState ADD CONSTRAINT UQ_UserState_id UNIQUE (id);
ALTER TABLE State ADD CONSTRAINT UQ_State_state UNIQUE (state);

--------------------------------------------------------------------
--DROP TABLE Gamer_Sate;
CREATE TABLE Gamer_State ( 
	gamer_id int NOT NULL,
	state_id int NOT NULL
);

ALTER TABLE Gamer_State ADD CONSTRAINT FK_Gamer_id
	FOREIGN KEY (gamer_id) REFERENCES Gamer (id);
ALTER TABLE Gamer_State ADD CONSTRAINT FK_State_id
	FOREIGN KEY (state_id) REFERENCES State (id);

--------------------------------------------------------------------

--DROP TABLE Status;
CREATE TABLE Status ( 
	id  int NOT NULL GENERATED ALWAYS AS IDENTITY 
		(START WITH 1, INCREMENT BY 1),
	status CHAR(20) NOT NULL
);

ALTER TABLE Status ADD CONSTRAINT PK_Status_id PRIMARY KEY (id);
--ALTER TABLE GameStatus ADD CONSTRAINT UQ_GameStatus_id UNIQUE (id);
ALTER TABLE Status ADD CONSTRAINT UQ_Status_status UNIQUE (status);

-----------------------------------------------------------------------
--DROP TABLE Game_Satus;
CREATE TABLE Game_Status ( 
	id int NOT NULL GENERATED ALWAYS AS IDENTITY 
		(START WITH 1, INCREMENT BY 1),
	gamer_X_id int NOT NULL,
	gamer_O_id int NOT NULL,
	score int NOT NULL,
	status_id int NOT NULL
);

ALTER TABLE Game_Status ADD CONSTRAINT PK_Game_Status_id PRIMARY KEY (id);
ALTER TABLE Game_Status ADD CONSTRAINT FK_Game_Status_gamer_X_id
	FOREIGN KEY (gamer_X_id) REFERENCES Gamer (id);
ALTER TABLE Game_Status ADD CONSTRAINT FK_Game_Status_gamer_O_id
	FOREIGN KEY (gamer_O_id) REFERENCES Gamer (id);
ALTER TABLE Game_Status ADD CONSTRAINT FK_Game_Status_Status_id
	FOREIGN KEY (status_id) REFERENCES Status (id);

--------------------------------------------------------------------
-----------------  инициализация таблиц STATE и STATUS -------------
--------------------------------------------------------------------
insert into TICTAC.STATE (STATE) values (1, 'Свободен');
insert into TICTAC.STATE (STATE) values (2, 'В процессе игры');
insert into TICTAC.STATE (STATE) values (3, 'Не беспокоить');

insert into TICTAC.STATUS (STATUS) values ('Приглашаю');
insert into TICTAC.STATUS (STATUS) values ('Приглашён');
insert into TICTAC.STATUS (STATUS) values ('Согласие');
insert into TICTAC.STATUS (STATUS) values ('Согласен');
insert into TICTAC.STATUS (STATUS) values ('Отказано');
insert into TICTAC.STATUS (STATUS) values ('Отказать');

---------------------------------------------------------------------

