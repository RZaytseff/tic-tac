package ru.edison.tictactoe.server.message;

/**
 * Represents handshake message
 * @author RZaytsev
 */
public class HandshakeMessageBean extends MessageBean {
	
	private int gameId;
	private String player;

	public HandshakeMessageBean(int gameId, String player) {
		this.type = "handshake";
		this.gameId = gameId;
		this.player = player;
	}

	public String getType() {
		return "handshake";
	}
	
	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}
}
