package ru.edison.tictactoe.server.message;

import com.google.gson.Gson;

/**
 * Base class of message beans
 * @author RZaytsev
 */
public class MessageBean {
	
	protected String type;

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public String getType() {
		return type;
	}	
	
}
