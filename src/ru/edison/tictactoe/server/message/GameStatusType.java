package ru.edison.tictactoe.server.message;

public enum GameStatusType {
	 GAME_READY_TO_PLAY("ready", 1),
	 GAME_IN_PLAY("gaming", 2),
	 GAME_INVITE("invite", 3),
	 GAME_INVITED("invited", 4),
	 GAME_CONFIRM("confirm", 5),
	 GAME_SUPPORT("support", 6),
	 GAME_REFUSE("refuse", 7),
	 GAME_WIN("win", 8),
	 GAME_LOSE("lose", 9),
	 GAME_UNDEFINED("undefined", 99);
	 
	 private String value;
	 private int code;
	 
	 public String value() {
		 return value;
	 }

	 public static String value(int code) {
		 for(GameStatusType type: GameStatusType.values())
			 if(code == type.code) return type.value;
		 return GAME_UNDEFINED.value;
	 }

	 public String code() {
		 return value;
	 }
	 
	 public static int code(String value) {
		 for(GameStatusType type: GameStatusType.values())
			 if(value.equalsIgnoreCase(type.value)) return type.code;
		 return GAME_UNDEFINED.code;
	 }

	 GameStatusType(String value, int code) {
		 this.value = value;
		 this.code = code;
	 }
	 
	 public String toString() {
		 return value;
	 }
}

