package ru.edison.tictactoe.server.message;

public interface GamerPresenceType {
	String GAMER_READY_TO_PLAY = "ready";
	String GAMER_IN_PLAY_PROCESS = "gaming";
	String GAMER_DO_NOT_DISTURB = "busy";
	String GAMER_OFF_LINE = "offLine";
}
