package ru.edison.tictactoe.server.message;

import java.util.List;
import java.util.Map;

/**
 * @Author RZaytsev - Роман Витальевич
 */
public class FileContentMessageBean extends MessageBean {
	
	private String url;
	
	private String content;
	
	private List<String> list;
	
	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	private Map<String, String> dictionary;

	public FileContentMessageBean() {
		super();
		type = "fileContent";
	}
	
	public FileContentMessageBean(String url) {
		this();
		this.url = url;
	}

	public FileContentMessageBean(String url, String content) {
		this(url);
		this.content = content;
	}

	public FileContentMessageBean(String url, Map<String, String> dictionary) {
		this(url);
		this.type = "dictionary";
		this.dictionary = dictionary;
	}

	public FileContentMessageBean(String url, List<String> list) {
		this(url);
		this.type = "stringList";
		this.list = list;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, String> getDictionary() {
		return dictionary;
	}

	public void setDictionary(Map<String, String> dictionary) {
		this.type = "dictionary";
		this.dictionary = dictionary;
	}
	
}
