package ru.edison.tictactoe.server.message;

/**
 * Represents response message
 * @author Роман Витальевич
 */
public class OutgoingMessageBean extends MessageBean {

	private String opponent;
	private String gridId;
	private boolean winner;
	private boolean tied;
	
	public OutgoingMessageBean(String opponent, String grid, boolean winner, boolean tied) {
		this.type = "response";
		this.opponent = opponent;
		this.gridId = grid;
		this.winner = winner;
		this.tied = tied;
	}
	
	public String getType() {
		return "response";
	}
	
	public String getOpponent() {
		return opponent;
	}

	public String getGrid() {
		return gridId;
	}
	
	public boolean getWinner() {
		return winner;
	}
	
	public boolean getTied() {
		return tied;
	}
}
