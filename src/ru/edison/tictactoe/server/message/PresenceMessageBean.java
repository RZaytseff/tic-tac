package ru.edison.tictactoe.server.message;

import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

/**
 * Represents handshake message
 * @author RZaytsev
 */
public class PresenceMessageBean extends MessageBean {
	private final char d = 34;
	
	private String nic;
	private String player;
	private String state;
	private String score;
	private String status;
	private List<GameMessageBean> games;

	public PresenceMessageBean() {
		super();
		this.type = MessageType.GAMER_PRESENCE_STATE;
	}
	
	public PresenceMessageBean(String type) {
		super();
		this.type = type;
	}
	
	public PresenceMessageBean(String nic, String state) {
		this();
		this.nic = nic;
		this.state = state;
	}

	public PresenceMessageBean(String type, String nic, String state) {
		this(type);
		this.nic = nic;
		this.state = state;
	}

	public PresenceMessageBean(String type, String nic, String state, String status) {
		this(type, nic, state);
		this.status = status;
	}

	public PresenceMessageBean(String type, String nic, String state, String score, String status) {
		this(type, nic, state, status);
		this.score = score;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<GameMessageBean> getGames() {
		return games;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setGames(List<GameMessageBean> games) {
		this.games = games;
	}
	
	public GameMessageBean getGame(String gamerX, String gamerO) {
		if(nic != null && gamerX != null && nic.equalsIgnoreCase(gamerX) && games != null) {
			for(GameMessageBean game: games) {
				if(game != null && game.getGamerO()!=null && game.getGamerO().equalsIgnoreCase(gamerO))
					return game;
			}
		}
		return null;
	}
	
	@Override
	public String toJson() {
		return "{" + d + "type" + d + ":" + d + type + d 
				+ "," + d + "nic" + d + ":" + d + (nic != null ? nic : "") + d 
				+ "," 
				+ d + "state" + d + ":" + d + (state != null ? state : "") + d 
				+ "," 
				+ d + "score" + d + ":" + d + (score != null ? score : "") + d 
				+ "," 
				+ d + "status" + d + ":" + d + (status != null ? status : "") + d 
				+ "," 
				+ d + MessageType.GAME_STATE_LIST + d + ":" + d + (games == null ? "[]": new Gson().toJson(games)) +d
				+ "}";
	}
	
	public static void main(String[] argv) {
		PresenceMessageBean state = new PresenceMessageBean("Looser", GamerPresenceType.GAMER_DO_NOT_DISTURB);
		state.setGames(new LinkedList<GameMessageBean>());
		System.out.println(state.toJson());
	}
}
