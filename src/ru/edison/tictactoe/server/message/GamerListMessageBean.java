package ru.edison.tictactoe.server.message;

import java.util.ArrayList;
import java.util.List;

import ru.edison.tictactoe.db.schema.Gamer;

/**
 * Represents a message of Tic Tac Toe for all gamer's list registred in the game
 * @author RZaitsev
 */
public class GamerListMessageBean extends MessageBean {
	
	private static List<Gamer> gamerList;
	
	private static char tab = 9;
	
//	public GamerListMessageBean() {super();}

	public GamerListMessageBean(List<String> gamers) {
		type = "gamerList";
		gamerList = new ArrayList<Gamer>();
		if(gamers == null || gamers.size() == 0) return;
		
		int i = 0;
		String tabs = String.valueOf(tab);
		for(String gamer: gamers) {
			if(gamer.split(tabs) != null && gamer.split(tabs).length > 0) {
				int id = ++i;
				String nic  = gamer.split(tabs)[0];
				String name = null;
				if(gamer.split(tabs).length > 1) {
					name = gamer.split(tabs)[1];
				}
				Gamer g = new Gamer();
				g.setId(id);
				g.setNic(nic);
				g.setName(name);
				gamerList.add(g);
			}
		}
	}

	public String getType() {
		return "gamerList";
	}
	
	public List<Gamer> getGamerList() {
		return gamerList;
	}	
	
	@Override
	public String toJson() {
		char d = 34; 
		StringBuffer res = new StringBuffer("{" + d + "type" + d + ":" + d + type + d + "," + d + "gamerList" + d + ":[");
		if(gamerList != null && gamerList.size() > 0) {
			for(Gamer gamer : gamerList) 
				res.append("{")
					.append(d).append("id").append(d).append(":").append(gamer.getId()).append(",")
					.append(d).append("nic").append(d).append(":").append(d).append(gamer.getNic()).append(d).append(",")
					.append(d).append("name").append(d).append(":").append(d).append(gamer.getName() == null ? "" : gamer.getName()).append(d).append(",")
					.append(d).append("login").append(d).append(":").append(d).append(gamer.getLogin() == null ? "" : gamer.getLogin()).append(d).append(",")
					.append(d).append("password").append(d).append(":").append(d).append(gamer.getPassword() == null ? "" : gamer.getPassword()).append(d)
					.append("},");
		}
		res.deleteCharAt(res.length() - 1).append("]}");
		return res.toString();
	}

	public static String toJson(List<String> gamers) {
		char d = 34; 
		StringBuffer res = new StringBuffer("{" + d + "type" + d + ":" + d + "gamerList" + d + "," + d + "gamerList" + d + ":[");
		if(gamers != null && gamers.size() > 0) {
			for(String gamer : gamers) {
				if(gamer == null || gamer.length() < 3 || !gamer.contains(String.valueOf(tab))) continue;
				String nic = gamer.split(String.valueOf(tab))[0];
				String name = gamer.split(String.valueOf(tab))[1];
				res.append("{")
					.append(d).append("id").append(d).append(":").append(d).append("").append(d).append(",")
					.append(d).append("nic").append(d).append(":").append(d).append(nic).append(d).append(",")
					.append(d).append("name").append(d).append(":").append(d).append(name).append(d).append(",")
					.append(d).append("login").append(d).append(":").append(d).append("").append(d).append(",")
					.append(d).append("password").append(d).append(":").append(d).append("").append(d)
					.append("},");
			}
		}
		res.deleteCharAt(res.length() - 1).append("]}");
		return res.toString();
	}
}
