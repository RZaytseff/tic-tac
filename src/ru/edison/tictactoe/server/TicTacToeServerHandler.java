package ru.edison.tictactoe.server;

import static org.jboss.netty.handler.codec.http.HttpHeaders.isKeepAlive;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static ru.edison.tictactoe.server.message.GameOverMessageBean.Result.TIED;
import static ru.edison.tictactoe.server.message.GameOverMessageBean.Result.YOU_WIN;
import static ru.edison.tictactoe.server.message.TurnMessageBean.Turn.WAITING;
import static ru.edison.tictactoe.server.message.TurnMessageBean.Turn.YOUR_TURN;

//import java.util.*;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.util.CharsetUtil;

import com.google.gson.Gson;
import org.jboss.netty.handler.codec.http.websocketx.*;

import ru.edison.tictactoe.db.dao.FileDao;
import ru.edison.tictactoe.db.dao.GameDao;
import ru.edison.tictactoe.db.dao.GamerDao;
import ru.edison.tictactoe.db.schema.Gamer;
import ru.edison.tictactoe.game.Game;
import ru.edison.tictactoe.game.Game.Status;
import ru.edison.tictactoe.game.Player;
import ru.edison.tictactoe.game.Game.PlayerLetter;
import ru.edison.tictactoe.server.message.FileContentMessageBean;
import ru.edison.tictactoe.server.message.FileContentMessageBean;
import ru.edison.tictactoe.server.message.GameMessageBean;
import ru.edison.tictactoe.server.message.GameOverMessageBean;
import ru.edison.tictactoe.server.message.GamerListMessageBean;
import ru.edison.tictactoe.server.message.PresenceMessageBean;
import ru.edison.tictactoe.server.message.GamerPresenceType;
import ru.edison.tictactoe.server.message.HandshakeMessageBean;
import ru.edison.tictactoe.server.message.MessageBean;
import ru.edison.tictactoe.server.message.MessageType;
import ru.edison.tictactoe.server.message.PlayerMessageBean;
import ru.edison.tictactoe.server.message.OutgoingMessageBean;
import ru.edison.tictactoe.server.message.TurnMessageBean;

/**
 * Handles a server-side channel for a multiplayer game of Tic Tac Toe.
 * @Created RZaytsev
 * @author Роман Витальевич
 */
public class TicTacToeServerHandler extends SimpleChannelUpstreamHandler {
	
	private static Map<String, ChannelHandlerContext> gamerPresenceList;
	
	private static Map<Integer, Game> games = new HashMap<Integer, Game>();
	
	private static final String WEBSOCKET_PATH = "/websocket";
	
	private GameDao gameDao = new GameDao(); 
        
    private WebSocketServerHandshaker handshaker;

	/* (non-Javadoc)
	 * 
	 * An incoming message (event). Invoked when either a:
	 * 
	 * - A player navigates to the page. The initial page load triggers an HttpRequest. 
	 * We perform the WebSocket handshake 
	 * 		and assign them to a particular game.
	 * 
	 * - OR A player clicks on a tic tac toe square. The message contains who clicked 
	 * 		on which square (1 thru 9) and which game they're playing.
	 */
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
			throws Exception {
		Object msg = e.getMessage();
		if (msg instanceof HttpRequest) {
			handleHttpRequest(ctx, (HttpRequest) msg);
		} else if (msg instanceof WebSocketFrame) {
			handleWebSocketFrame(ctx, (WebSocketFrame) msg);
		}
	}

	/**
	 * Handles all HttpRequests. Must be a GET. Performs the WebSocket handshake 
	 * and assigns a player to a game.
	 * 
	 * @param ctx
	 * @param req
	 * @throws Exception
	 */
	private void handleHttpRequest(ChannelHandlerContext ctx, HttpRequest req)
																throws Exception {

		// Allow only GET methods.
		if (req.getMethod() != HttpMethod.GET) {
			sendHttpResponse(ctx, req, new DefaultHttpResponse(
					HttpVersion.HTTP_1_1, HttpResponseStatus.FORBIDDEN));
			return;
		}

          // Handshake
          WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory
        		  								(this.getWebSocketLocation(req), null, false);
          this.handshaker = wsFactory.newHandshaker(req);
          if (this.handshaker == null) {
              wsFactory.sendUnsupportedWebSocketVersionResponse(ctx.getChannel());
         } else {
              
		       this.handshaker.handshake(ctx.getChannel(), req);
		       
		       List<String> passiveGamers = new LinkedList<String>();
		       for(String gamer: GamerDao.getGamerList()) {
		    	   String nic = gamer.split(String.valueOf('\u0009'))[0];
		    	   if(getPresenceState(nic) == null 
		    			   || GamerPresenceType.GAMER_OFF_LINE.equalsIgnoreCase(getPresenceState(nic))) {
		    		   passiveGamers.add(gamer);
		    	   }
		       }
		       
		       ctx.getChannel().write(new TextWebSocketFrame(GamerListMessageBean.toJson(passiveGamers)));
              
//            initGame(ctx);
         }
	}
	

	private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
            
        // Check for closing frame
        if (frame instanceof CloseWebSocketFrame) {
        	if(getGamerNic(ctx) != null) // пользователь успел авторизоваться
        		changeGamerState(getGamerNic(ctx), GamerPresenceType.GAMER_OFF_LINE); 
            this.handshaker.close(ctx.getChannel(), (CloseWebSocketFrame) frame);
            return;
        } else if (frame instanceof PingWebSocketFrame) {
            ctx.getChannel().write(new PongWebSocketFrame(frame.getBinaryData()));
            return;
        } else if (!(frame instanceof TextWebSocketFrame)) {
            throw new UnsupportedOperationException
            			(String.format("%s frame types not supported", frame.getClass()
                    .getName()));
        }
		
		Gson gson = new Gson();
		MessageBean message = gson.fromJson(((TextWebSocketFrame) frame).getText()
																		, MessageBean.class);
		String messageType = message.getType();
		
		/**
		 *  сервис получения словарей с сервера различных языков 
		 */
		if(MessageType.GET_DICTIONARY.equalsIgnoreCase(messageType)) {
			FileContentMessageBean fc = gson.fromJson(((TextWebSocketFrame) frame).getText(), FileContentMessageBean.class);
			try {
				ctx.getChannel().write(new TextWebSocketFrame(new FileDao().getStringList(fc.getUrl()).toJson()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if(MessageType.GET_DICTIONARY_LIST.equalsIgnoreCase(messageType)) {
			FileContentMessageBean fc = gson.fromJson(((TextWebSocketFrame) frame).getText(), FileContentMessageBean.class);
			try {
				ctx.getChannel().write(new TextWebSocketFrame(new FileDao().getStringList(fc.getUrl()).toJson()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 *  сервис получения произвольных данных с сервера
		 */
		if(MessageType.GET_FILE.equalsIgnoreCase(messageType)) {
			FileContentMessageBean fc = gson.fromJson(((TextWebSocketFrame) frame).getText(), FileContentMessageBean.class);
			try {
				ctx.getChannel().write(new TextWebSocketFrame(new FileDao().getFileContent(fc.getUrl()).toJson()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(MessageType.REGISTRATION.equalsIgnoreCase(messageType)) {
			Gamer gamer = gson.fromJson(((TextWebSocketFrame) frame).getText(), Gamer.class);
			GamerDao.addNewGamer(gamer);
			
		/** сразу после регистрации - переход к процедуре логина */
			messageType = MessageType.LOGIN;
		}
		
		if(MessageType.LOGIN.equalsIgnoreCase(messageType)) {
			Gamer gamer = gson.fromJson(((TextWebSocketFrame) frame).getText(), Gamer.class);
			gamer.setState(GamerPresenceType.GAMER_READY_TO_PLAY);
			gamerLogin(ctx, gamer);
		}
		
		if(MessageType.GAMER_PRESENCE_STATE_LIST.equalsIgnoreCase(messageType)) {
			ctx.getChannel().write(new TextWebSocketFrame(presenceListJson(getGamerNic(ctx))));
		}
/**
 *  изменить состояние игрока: 
 *  1. присутствие в он-лайн
 *  2. счёт той или иной игры
 *  3. статус приглашения к игре от того/к тому или иного игрока/игроку		
 */
		if(MessageType.GAMER_PRESENCE_STATE.equalsIgnoreCase(messageType)) {
			GameMessageBean game = gson.fromJson(((TextWebSocketFrame) frame).getText(), GameMessageBean.class);
			String nic = getGamerNic(ctx);
			String player = game.getGamerO();
			changeGamerState(ctx, game.getState());
			int scoreX =  Integer.parseUnsignedInt(game.getScore().split(":")[0]);
			int scoreO =  Integer.parseUnsignedInt(game.getScore().split(":")[1]);
			changeGameScore(nic, player, scoreX);
			changeGameScore(player, nic, scoreO);
			sendGameStateAlert(ctx, game);
		}
		
/**
 *  изменить состояние игрока: только
 *  1. присутствие в он-лайн
 */
		if(MessageType.GAMER_STATE.equalsIgnoreCase(messageType)) {
			Gamer gamer = gson.fromJson(((TextWebSocketFrame) frame).getText(), Gamer.class);
			String nic = getGamerNic(ctx);
			changeGamerState(nic, gamer.getState());
//			sendPresenceStateAlert(nic, gamer.getState());
		}
		
/**
 *  изменить состояние игрока: только
 *  2. счёт той или иной игры
 */
		if(MessageType.GAME_OVER.equalsIgnoreCase(messageType)) {
			GameMessageBean game = gson.fromJson(((TextWebSocketFrame) frame).getText(), GameMessageBean.class);
			String player = game.getGamerO();
			String state = game.getState();
			int newScore;
			if(MessageType.GAME_OVER_WIN.equalsIgnoreCase(state)) {
				newScore = increaseGameScore(ctx, player);
			} else if(MessageType.GAME_OVER_LOSE.equalsIgnoreCase(state)){
				newScore = decreaseGameScore(ctx, player);
			} else {
				
			}
				
		}
		
/**
 *  изменить состояние игрока: только
 *  3. статус приглашения к игре от того/к тому или иного игрока/игроку	
 */
		if(MessageType.GAME_STATUS.equalsIgnoreCase(messageType)) {
			GameMessageBean game = gson.fromJson(((TextWebSocketFrame) frame).getText(), GameMessageBean.class);
			String nic = getGamerNic(ctx);
			game.setGamerX(nic);	
			changeGameStatus(ctx, game.getGamerO(), game.getStatus());
			sendGameStateAlert(nic, game);
		}
		
// getters of various info's		
		if(MessageType.GET_GAMER_STATE.equalsIgnoreCase(messageType)) {
			String nic = gson.fromJson(((TextWebSocketFrame) frame).getText(), Gamer.class).getNic();
			sendGamerState(nic, getGamerState(nic));
		}
		
		if(MessageType.GET_GAME_SCORE.equalsIgnoreCase(messageType)) {
			GameMessageBean game = gson.fromJson(((TextWebSocketFrame) frame).getText(), GameMessageBean.class);
			sendGameScore(game.getGamerX(), 
					gameDao.getScore(game.getGamerX(), game.getGamerO())
					+ ":"
					+ gameDao.getScore(game.getGamerO(), game.getGamerX())
					);
		}
		
		if(MessageType.GET_GAME_STATUS.equalsIgnoreCase(messageType)) {
			GameMessageBean game = gson.fromJson(((TextWebSocketFrame) frame).getText(), GameMessageBean.class);
			sendGameStatus(game.getGamerX(), gameDao.getStatus(game.getGamerX(), game.getGamerO()));
		}
		
		// invitation handshake protocol procedures
		if(MessageType.GAME_INVITATION.equalsIgnoreCase(messageType)) {
			PresenceMessageBean presence = gson.fromJson(((TextWebSocketFrame) frame).getText(), PresenceMessageBean.class);
			String inviteStatus = presence.getStatus();
			String fromGamer = getGamerNic(ctx);
			String toGamer = presence.getNic();
			
			
			if(MessageType.GAME_INVITE.equalsIgnoreCase(inviteStatus)) {
				gameDao.setStatus(fromGamer, toGamer, MessageType.GAME_INVITE); 
//				gameDao.setStatus(toGamer, fromGamer, MessageType.GAME_INVITED); 
				sendInviteStatus(fromGamer, toGamer, MessageType.GAME_INVITED);
			}
			
			if(MessageType.GAME_CONFIRM_INVITE.equalsIgnoreCase(inviteStatus)) {
				gameDao.setStatus(fromGamer, toGamer, MessageType.GAME_CONFIRM_INVITE); 
//				gameDao.setStatus(toGamer, fromGamer, MessageType.GAME_CONFIRM_INVITE); 
				sendInviteStatus(fromGamer, toGamer, MessageType.GAME_CONFIRM_INVITE);
				// начать новую игру и перейти в состояние ожидания
	            initGame(ctx, new Game());
			}
			
			if(MessageType.GAME_CONFIRM_INVITED.equalsIgnoreCase(inviteStatus)) {
				gameDao.setStatus(toGamer, fromGamer, MessageType.GAME_CONFIRM_INVITED); 
//				gameDao.setStatus(toGamer, fromGamer, MessageType.GAME_CONFIRM_INVITE); 
//				sendInviteStatus(fromGamer, toGamer, MessageType.GAME_CONFIRM_INVITE);
				
				// начать игру c ожидающим оппонентом
				initGame(ctx, findWaitingGame(toGamer));
			}
			
			if(MessageType.GAME_REFUSE_INVITE.equalsIgnoreCase(inviteStatus)) {
				gameDao.setStatus(fromGamer, toGamer, MessageType.GAME_REFUSE_INVITE); 
				gameDao.setStatus(toGamer, fromGamer, MessageType.GAME_REFUSE_INVITE); 
				sendInviteStatus(fromGamer, toGamer, MessageType.GAME_REFUSE_INVITE);
			}
		}
		
		if(MessageType.GAME.equalsIgnoreCase(messageType)) {
			PlayerMessageBean playerMessage = gson.fromJson(((TextWebSocketFrame) frame).getText()
																		, PlayerMessageBean.class);
			handleGameFrame(ctx, playerMessage);
			// was done before when players are handshaking
//			changeGamerState(playerMessage.getNic(), GamerPresenceType.GAMER_IN_PLAY_PROCESS);
		}
		return;
}
		

	private void sendHttpResponse(ChannelHandlerContext ctx, HttpRequest req,
			HttpResponse res) {
		// Generate an error page if response status code is not OK (200).
		if (res.getStatus().getCode() != 200) {
			res.setContent(ChannelBuffers.copiedBuffer(res.getStatus()
					.toString(), CharsetUtil.UTF_8));
			changeGamerState(getGamerNic(ctx), GamerPresenceType.GAMER_OFF_LINE);
		}

		// Send the response and close the connection if necessary.
		ChannelFuture f = ctx.getChannel().write(res);
		
		if (!isKeepAlive(req) || res.getStatus().getCode() != 200) {
			f.addListener(ChannelFutureListener.CLOSE);
			changeGamerState(getGamerNic(ctx), GamerPresenceType.GAMER_OFF_LINE);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
			throws Exception {
		e.getCause().printStackTrace();
		e.getChannel().close();
	}
	
	private void sendInviteStatus(String fromGamer, String toGamer, String status) {
		getGamerChannel(toGamer).write(new TextWebSocketFrame
				(new PresenceMessageBean(MessageType.GAME_INVITATION, fromGamer, getGamerState(toGamer) , status).toJson()));	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private HashMap getContextAttachment(ChannelHandlerContext ctx) {
		if(ctx == null) {
			return null;
		}
		HashMap attach = (HashMap<String, Object>)ctx.getAttachment();
		if(attach == null) {
			attach = new HashMap<String, Object>();
			ctx.setAttachment(attach);
		}
		return attach;
	}

	public synchronized void gamerLogin(ChannelHandlerContext ctx, Gamer gamer) {
		gamer.setState(GamerPresenceType.GAMER_READY_TO_PLAY);
		setContextAttachment(ctx, "gamer", gamer);
		setContextAttachment(ctx, "presence", GamerPresenceType.GAMER_READY_TO_PLAY);
		//запомнить себя как готового к игре 
		getGamerPresenceList().put(gamer.getNic(), ctx);
		// рассылка оповещений о своём присутствии и готовности к игре всем он-лайн геймерам
		sendPresenceStateAlert(gamer.getNic(), GamerPresenceType.GAMER_READY_TO_PLAY);
	}
	

	
	
	private String getPresenceState(String key) {
		return (String) getContextAttachment(getGamerPresenceList().get(key), "presence");
	}

	// getter's of gamer's presence state and some gamer's game
	private GameMessageBean getGameState(String playerX, String playerO) {
		GameMessageBean game = new GameMessageBean(playerX, playerO);
		game.setScore(gameDao.getScore(playerX, playerO) + ":" + gameDao.getScore(playerO, playerX));
		game.setStatus(gameDao.getStatus(playerX, playerO));
		return game;
	}

	private String getGamerState(String nic) {
		return getPresenceState(nic);
	}

	private String getGameScore(String playerX, String playerO) {
		return gameDao.getScore(playerX, playerO) + ":" + gameDao.getScore(playerO, playerX);
	}
	
	private String getGameStatus(String playerX, String playerO) {
		return gameDao.getStatus(playerX, playerO);
	}
	
	/** gameStae include gamerState + gameScore + gameStatus */
	private void sendGameState(String toGamer /* to whom send */ , GameMessageBean gameState) {
		getGamerChannel(toGamer).write(new TextWebSocketFrame(gameState.toJson()));
	}

	/** send  gamerState only */
	private void sendGamerState(String toGamer /* to whom send */ , String state) {
		sendGameState(toGamer , new GameMessageBean(toGamer, null, state));
	}

	/** send gameScore only */
	private void sendGameScore(String toGamer , String score) {
		sendGameState(toGamer , new GameMessageBean(toGamer, null, null, score, null));
	}

	/** send gameStatus only */
	private void sendGameStatus(String toGamer , String status) {
		sendGameState(toGamer , new GameMessageBean(toGamer, null, null, null, status));
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private PresenceMessageBean getGamerPresence(String nic) {
		PresenceMessageBean gamerPresence = new PresenceMessageBean(nic
				, (String)((Map<String, Object>)getGamerPresenceList().get(nic).getAttachment()).get("presence"));
		gamerPresence.setGames(gameDao.getGameStatusList(nic));
		return gamerPresence;
	}
	
	/** send gamer presence state and his game's List result & state to himself */
	@SuppressWarnings("unused")
	private void sendGamerPresence(PresenceMessageBean gamerPresence) {
		getGamerChannel(gamerPresence.getNic()).write(new TextWebSocketFrame
														(gamerPresence.toJson()));
	}

// changer's of presence state and status of some game for current context gamer 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private synchronized void changeGamerState(ChannelHandlerContext ctx, String state) {
		((Gamer)((Map)getGamerPresenceList().get(getGamerNic(ctx)).getAttachment()).get("gamer")).setState(state);
		((Map<String, Object>)getGamerPresenceList().get(getGamerNic(ctx)).getAttachment()).put("presence", state);
		setContextAttachment(getGamerPresenceList().get(getGamerNic(ctx)), "presence", state);
	}

	@SuppressWarnings("unused")
	private void changeGameScore(ChannelHandlerContext ctx, String player, int score) {
		gameDao.setScore(getGamerNic(ctx), player, score);
	}
	
	private void changeGameScore(String nic, String player, int score) {
		gameDao.setScore(nic, player, score);
	}
	
	private int increaseGameScore(ChannelHandlerContext ctx, String player) {
		return gameDao.incrementScore(getGamerNic(ctx), player);
	}
	
	private int decreaseGameScore(ChannelHandlerContext ctx, String player) {
		return gameDao.incrementScore(player, getGamerNic(ctx));
	}
	
	private void changeGameStatus(ChannelHandlerContext ctx, String player, String status) {
		gameDao.setStatus(getGamerNic(ctx), player, status);;
	}
	
	private void sendGameStateAlert(ChannelHandlerContext ctx, GameMessageBean gameState) {
		sendGameStateAlert(getGamerNic(ctx), gameState);
	}
	
	private void sendGameStateAlert(String nicName, GameMessageBean gameState) {
		for(String nic: getGamerPresenceList().keySet()) {
			if(!nic.equalsIgnoreCase(nicName) 
					&& !GamerPresenceType.GAMER_OFF_LINE.equalsIgnoreCase(getPresenceState(nic))) {
				getGamerChannel(nic).write(new TextWebSocketFrame(gameState.toJson()));
			}
			
		}
	}

	private void changeGamerState(String nic, String state) {
		changeGamerState(getGamerPresenceList().get(nic), state);
		sendPresenceStateAlert(nic, state);
	}

	@SuppressWarnings("unused")
	private void sendPresenceStateAlert(ChannelHandlerContext ctx, String state) {
		sendPresenceStateAlert(getGamerNic(ctx), state);
	}

	private void sendPresenceStateAlert(String nicName, String state) {
		for(String nic: getGamerPresenceList().keySet()) {
			if(!nic.equalsIgnoreCase(nicName) 
					&& !GamerPresenceType.GAMER_OFF_LINE.equalsIgnoreCase(getPresenceState(nic))) {
				getGamerChannel(nic).write
						(new TextWebSocketFrame
								(new PresenceMessageBean
										(MessageType.GAMER_PRESENCE_STATE
												, nicName, state
												, "" + gameDao.getScore(nic, nicName) + ":" + gameDao.getScore(nicName, nic)
												, gameDao.getStatus(nic, nicName)
												).toJson()));
			}
			
		}
	}
		
	private synchronized Map<String, ChannelHandlerContext> getGamerPresenceList() {
		if(gamerPresenceList == null) {
			gamerPresenceList = new HashMap<String, ChannelHandlerContext>();
		}
		return gamerPresenceList;
			
	}
	
	private String getGamerNic(ChannelHandlerContext ctx) {
		return getContextAttachment(ctx) == null || getContextAttachment(ctx).get("gamer") == null ? null
				: (String) ((Gamer) getContextAttachment(ctx).get("gamer")).getNic();
	}
	private Channel getGamerChannel(String key) {
		return getGamerPresenceList().get(key).getChannel();
	}

	private Object getContextAttachment(ChannelHandlerContext ctx, String key) {
		return getContextAttachment(ctx) == null ? null : getContextAttachment(ctx).get(key);
	}

	@SuppressWarnings("unchecked")
	private synchronized void setContextAttachment(ChannelHandlerContext ctx, String key, Object value) {
		if(getContextAttachment(ctx) == null) {
			ctx.setAttachment(new HashMap<String, Object>());
		}
		((Map<String, Object>)getContextAttachment(ctx)).put(key, value);
	}

	private String presenceListJson(String gamerNic) {
		char d = 34; 
		StringBuffer res = new StringBuffer("{" + d + "type" + d + ":" 
							+ d + MessageType.GAMER_PRESENCE_STATE_LIST + d + "," 
							+ d + MessageType.GAMER_PRESENCE_STATE_LIST + d + ":[");
		if(getGamerPresenceList().size() > 0) {
			for(String nic : getGamerPresenceList().keySet()) {
				if(nic.equalsIgnoreCase(gamerNic) || getPresenceState(nic) == null
						|| GamerPresenceType.GAMER_OFF_LINE.equalsIgnoreCase(getPresenceState(nic))) continue;
				res.append("{")
					.append(d).append("nic").append(d).append(":").append(d).append(nic).append(d).append(",")
					.append(d).append("score").append(d).append(":").append(d)
						.append(gameDao.getScore(gamerNic, nic)).append(":").append(gameDao.getScore(nic, gamerNic)) 
						.append(d).append(",")
					.append(d).append("status").append(d).append(":")
						.append(d).append(gameDao.getStatus(gamerNic, nic)).append(d).append(",")
					.append(d).append("state").append(d).append(":").append(d).append(getPresenceState(nic)).append(d)
					.append("},");
			}
			res.deleteCharAt(res.length() - 1);
		}
		res.append("]}");
		return res.toString();
		
	}

	private String getWebSocketLocation(HttpRequest req) {
		return "ws://" + req.getHeader(HttpHeaders.Names.HOST) + WEBSOCKET_PATH;
	}
	/**
	 * Initializes a game. Finds an open game for a player 
	 * (if another player is already waiting) or creates a new game.
	 * 
	 * @param ctx
	 */
	private void initGame(ChannelHandlerContext ctx, Game game) {
		// Create a new instance of player and assign their channel for WebSocket communications.
		Player player = new Player(ctx.getChannel(), getGamerNic(ctx));
		
		// Add the player to the game.
		Game.PlayerLetter letter = game.addPlayer(player);
		
		// Add the game to the collection of games.
		games.put(game.getId(), game);
		
		// Send confirmation message to player with game ID and their assigned letter (X or O) 
		ctx.getChannel().write(new TextWebSocketFrame(new HandshakeMessageBean(game.getId(), letter.toString()).toJson()));
		
		// If the game has begun we need to inform the players. Send them a "turn" message (either "waiting" or "your_turn")
		if (game.getStatus() == Game.Status.IN_PROGRESS) {			
			game.getPlayer(PlayerLetter.O).getChannel().write(new TextWebSocketFrame(new TurnMessageBean(WAITING).toJson()));
		} else if(game.getStatus() == Game.Status.WAITING) {			
			game.getPlayer(PlayerLetter.X).getChannel().write(new TextWebSocketFrame(new TurnMessageBean(YOUR_TURN).toJson()));
		}
	}
	
	/**
	 * Finds an open game for a player (if another player is waiting) or creates a new game.
	 *
	 * @return Game
	 */
	private Game findWaitingGame(String nic) {		
		// Find an existing game and return it
		for (Game game : games.values()) {
			if (Game.Status.WAITING.equals(game.getStatus(nic)))
				return game;
        }		
		// Or return a new game
		return new Game();
	}

	private void waitingGameListSupport(ChannelHandlerContext ctx) {
		/** Try to find a game list waiting for a player. 
		 * If one doesn't exist, create a new one.
		 */
//		if(games == null) games = new HashMap<Integer, Game>(); // because it created during init the class
		for (Game game : games.values()) {
			if (Game.Status.WAITING.equals(game.getStatus(getGamerNic(ctx)))) {
				/** Create a new instance of player 
				 * and assign their channel for WebSocket communications.
				 */
				Player player = new Player(ctx.getChannel(), getGamerNic(ctx));
				
				// Add the player to the game.
				Game.PlayerLetter letter = game.addPlayer(player);
				
				// Add the game to the collection of games.
				addGame(ctx, game);
				
				/** Send confirmation message to player with game ID 
				 * and their assigned letter (X or O) 
				 */
				ctx.getChannel().write(new TextWebSocketFrame(
							new HandshakeMessageBean(game.getId(), letter.toString()).toJson()));
				
				/** If the game has begun we need to inform the players. 
				 * Send them a "turn" message (either "waiting" or "your_turn")
				 */
				if (game.getStatus() == Game.Status.IN_PROGRESS) {			
					game.getPlayer(PlayerLetter.X).getChannel().write(new TextWebSocketFrame
							(new TurnMessageBean(YOUR_TURN).toJson()));
					game.getPlayer(PlayerLetter.O).getChannel().write(new TextWebSocketFrame
							(new TurnMessageBean(WAITING).toJson()));
				}
			}
		}
	}

	/**
	 * Finds an open game for a player (if another player is waiting)
	 *  or creates a new game.
	 *
	 * @return Game
	 */
	private synchronized Map<Integer, Game> addGame(ChannelHandlerContext ctx, Game game) {		
		games.put(game.getId(), game);
		return games;
	}

	/**
	 * Process turn data from players. 
	 * Message contains which square they clicked on. 
	 * Sends turn data to their opponent.
	 * 
	 * @param ctx
	 * @param frame
	 */
	private void handleGameFrame(ChannelHandlerContext ctx, PlayerMessageBean message) {
            
		Game game = games.get(message.getGameId());
		Player opponent = game.getOpponent(message.getPlayer());
		Player player = game.getPlayer(PlayerLetter.valueOf(message.getPlayer()));
		
		// Mark the cell the player selected.
		game.markCell(message.getGridIdAsInt(), player.getLetter());
		
		// Get the status for the current game.
		boolean winner = game.isPlayerWinner(player.getLetter());
		boolean tied = game.isTied();
		
		// increase player score in database
		if(game.getStatus().equals(Game.Status.WON) && !tied) {
			String nicWin;
			String nicLose;
			if(winner) {
				nicWin = player.getNic();
				nicLose = opponent.getNic();
			} else {
				nicWin = opponent.getNic();
				nicLose = player.getNic();
			}
			gameDao.incrementScore(nicWin, nicLose);
		}
		
		// Respond to the opponent in order to update their screen.
		String responseToOpponent = new OutgoingMessageBean(player.getLetter().toString()
											, message.getGridId(), winner, tied).toJson();		
		opponent.getChannel().write(new TextWebSocketFrame(responseToOpponent));
		
		// Respond to the player to let them know they won.
		if (winner) {
			player.getChannel().write(new TextWebSocketFrame(new GameOverMessageBean(YOU_WIN).toJson()));
		} else if (tied) {
			player.getChannel().write(new TextWebSocketFrame(new GameOverMessageBean(TIED).toJson()));
		}
	}
}