package ru.edison.tictactoe.db.schema;

/**
 * @created RZaytseff 
 * @author Роман Витальевич
 * @Date 01.07.2015 16:11:36
 * @comment
 * @version 1.0
 */
public class GamerState {

	private int gamer_id;
	public static int state_id;
	
	private int getGamer_id() {
		return gamer_id;
	}
	public void setGamer_id(int gamer_id) {
		this.gamer_id = gamer_id;
	}
	public int getState_id() {
		return state_id;
	}
	public void setState_id(int state_id) {
		this.state_id = state_id;
	}
}