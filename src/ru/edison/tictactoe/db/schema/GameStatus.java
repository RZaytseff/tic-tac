package ru.edison.tictactoe.db.schema;

/**
 * @created RZaytseff
 * @author Роман Витальевич 
 * @Date 01.07.2015 16:11:36
 * @comment
 * @version 1.0
 */
public class GameStatus {

	private int id;
	private int gamer_X_id;
	private int gamer_O_id;
	private int score;
	private int status_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGamer_X_id() {
		return gamer_X_id;
	}
	public void setGamer_X_id(int gamer_X_id) {
		this.gamer_X_id = gamer_X_id;
	}
	public int getGamer_O_id() {
		return gamer_O_id;
	}
	public void setGamer_O_id(int gamer_O_id) {
		this.gamer_O_id = gamer_O_id;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getStatus_id() {
		return status_id;
	}
	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}

}