package ru.edison.tictactoe.db.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;

import ru.edison.tictactoe.db.schema.Gamer;
import ru.edison.tictactoe.server.message.GamerListMessageBean;

public class GamerDao {
	
	private static String GAMER_LIST_FILE_NAME = "db/GamerList.txt";
	
	private static File GAMER_LIST_FILE;
	
	private static LinkedList<String> gamerList;
	
	private final static char tab = 9;
	private final static char cr = 13;
	private final static char lf = 10;
	
	// метод статический для удобства и для лёгкой синхронизации, протому что принято роешение не использовать в проекте Dependence Injection 
	// в нормальном проекте так лучше не делать...
	public static synchronized LinkedList<String> getGamerList() throws IOException {
		File f;
        // read all gamers nic's and names
        	if(gamerList == null) {
      		  gamerList = new LinkedList<String>();
	          f = new File(GAMER_LIST_FILE_NAME);
	          if(f.exists() && f.canRead()) {
	        	  GAMER_LIST_FILE = f;
	          } else {
           		  if(f.createNewFile()) {
           			  GAMER_LIST_FILE = f;
           		  } else {
           			  GAMER_LIST_FILE_NAME = "New_" + GAMER_LIST_FILE_NAME;
	      	          f = new File(GAMER_LIST_FILE_NAME);
	    	          if(f.exists()) {
	    	        	  throw new IOException("Error reding Gamer List & attempt create new list was fault!");
	    	          } else {
	    	        	  GAMER_LIST_FILE = f;
	               		  if(f.createNewFile()) {
	               			  GAMER_LIST_FILE = f;
	               		  } else {
	               			 throw new IOException("Error reding Gamer List & attempt create new list was fault!");
	               		  }
	           		  }
           		  }
	          }
              BufferedReader br = new BufferedReader(	//new FileReader(GAMER_LIST_FILE));
            		  new InputStreamReader(new FileInputStream(GAMER_LIST_FILE_NAME), "UTF-8"));
//              String line;
              
              // if gamer name start with symbol # then the gamer is actually remove
              for (String line = br.readLine(); line != null; line = br.readLine())
            	  if(!line.startsWith("#")) gamerList.add(line);
        	}
            return gamerList;
	}
	
	public static synchronized Boolean addNewGamer(Gamer gamer) {
		String newGamer = "" + lf + gamer.getNic() + tab + gamer.getName();
		try {
			getGamerList().add(newGamer);
		} catch (IOException e) {
			return null;
		}
        // add new gamer to the end of file
		try {
			BufferedWriter fot = new BufferedWriter(	//	new FileWriter(GAMER_LIST_FILE, true));
					new OutputStreamWriter(new FileOutputStream(GAMER_LIST_FILE_NAME, true), "UTF-8"));
			fot.append(newGamer);
			fot.flush();
			fot.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}


	public static void main(String[] args) throws IOException {
          System.out.println(new GamerListMessageBean(getGamerList()).toJson());
          Gamer newGamer = new Gamer();
          newGamer.setNic("Mity");
          newGamer.setName("Micky Mouse");
          addNewGamer(newGamer);
          System.out.println(new GamerListMessageBean(getGamerList()).toJson());
          
	}

}
