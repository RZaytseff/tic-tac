package ru.edison.tictactoe.db.dao;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ru.edison.tictactoe.server.message.FileContentMessageBean;

public class FileDao {
	
	private static String DICTIONARY_RU = "languages/ru.json";
	
	private final static char tab = 9;
	private final static char cr = 13;
	private final static char lf = 10;

	public FileContentMessageBean getFileContent(String url) throws IOException {
	
		StringBuffer sb = new StringBuffer("");

		  BufferedReader br = new BufferedReader(	//new FileReader(GAMER_LIST_FILE));
		  new InputStreamReader(new FileInputStream(url), "UTF-8"));;
		
		  for (String line = br.readLine(); line != null; line = br.readLine())
			  if(!line.startsWith("#")) sb.append(line);
		  
		return new FileContentMessageBean(url, sb.toString());
	}
	

	public FileContentMessageBean getDictionary(String url) throws IOException {
		
		Map<String, String> dict = new HashMap<String, String>();

		  BufferedReader br = new BufferedReader(	//new FileReader(GAMER_LIST_FILE));
		  new InputStreamReader(new FileInputStream(url), "UTF-8"));;
		
		  for (String line = br.readLine(); line != null; line = br.readLine())
			  if(!line.startsWith("#") && !line.startsWith("//") && line.contains("=")) {
				  dict.put(line.split("=")[0].trim(), line.split("=")[1].trim());
			  }
		  
		return new FileContentMessageBean(url, dict);
	}
	

	public FileContentMessageBean getStringList(String url) throws IOException {
		
		List<String> list = new LinkedList<String>();

		  BufferedReader br = new BufferedReader(	//new FileReader(GAMER_LIST_FILE));
		  new InputStreamReader(new FileInputStream(url), "UTF-8"));;
		
		  for (String line = br.readLine(); line != null; line = br.readLine())
			  if(line.length() > 0 && !line.startsWith("#")) {
				  list.add(line);
			  }
		  
		return new FileContentMessageBean(url, list);
	}
	
	public static void main(String[] args) throws IOException {
          System.out.println(new FileDao().getFileContent(DICTIONARY_RU).getContent());
          
	}

}
