package ru.edison.tictactoe.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

public class Transliterator {

    private static final Map<Character, String> ruLatMap = new HashMap<Character, String>();

    static {
        ruLatMap.put('А', "A");
        ruLatMap.put('Б', "B");
        ruLatMap.put('В', "V");
        ruLatMap.put('Г', "G");
        ruLatMap.put('Д', "D");
        ruLatMap.put('Е', "E");
        ruLatMap.put('Ё', "Yo");
        ruLatMap.put('Ж', "Zh");
        ruLatMap.put('З', "Z");
        ruLatMap.put('И', "I");
        ruLatMap.put('Й', "YI");
        ruLatMap.put('К', "K");
        ruLatMap.put('Л', "L");
        ruLatMap.put('М', "M");
        ruLatMap.put('Н', "N");
        ruLatMap.put('О', "O");
        ruLatMap.put('П', "P");
        ruLatMap.put('Р', "R");
        ruLatMap.put('С', "S");
        ruLatMap.put('Т', "T");
        ruLatMap.put('У', "U");
        ruLatMap.put('Ф', "F");
        ruLatMap.put('Х', "H");
        ruLatMap.put('Ц', "C");
        ruLatMap.put('Ч', "Ch");
        ruLatMap.put('Ш', "Sh");
        ruLatMap.put('Щ', "Chsh");
        ruLatMap.put('Ъ', "'");
        ruLatMap.put('Ы', "Y");
        ruLatMap.put('Ь', "'");
        ruLatMap.put('Э', "Ye");
        ruLatMap.put('Ю', "Yu");
        ruLatMap.put('Я', "Ya");
        ruLatMap.put('а', "a");
        ruLatMap.put('б', "b");
        ruLatMap.put('в', "v");
        ruLatMap.put('г', "g");
        ruLatMap.put('д', "d");
        ruLatMap.put('е', "e");
        ruLatMap.put('ё', "yo");
        ruLatMap.put('ж', "zh");
        ruLatMap.put('з', "z");
        ruLatMap.put('и', "i");
        ruLatMap.put('й', "yi");
        ruLatMap.put('к', "k");
        ruLatMap.put('л', "l");
        ruLatMap.put('м', "m");
        ruLatMap.put('н', "n");
        ruLatMap.put('о', "o");
        ruLatMap.put('п', "p");
        ruLatMap.put('р', "r");
        ruLatMap.put('с', "s");
        ruLatMap.put('т', "t");
        ruLatMap.put('у', "u");
        ruLatMap.put('ф', "f");
        ruLatMap.put('х', "h");
        ruLatMap.put('ц', "c");
        ruLatMap.put('ч', "ch");
        ruLatMap.put('ш', "sh");
        ruLatMap.put('щ', "chsh");
        ruLatMap.put('ъ', "'");
        ruLatMap.put('ы', "y");
        ruLatMap.put('ь', "'");
        ruLatMap.put('э', "ye");
        ruLatMap.put('ю', "yu");
        ruLatMap.put('я', "ya");

    }

    public static String ru_lat(String string) {
        StringBuilder transliteratedString = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            Character ch = string.charAt(i);
            String charFromMap = ruLatMap.get(ch);
            if (charFromMap == null) {
                transliteratedString.append(ch);
            } else {
                transliteratedString.append(charFromMap);
            }
        }
        return transliteratedString.toString();
    }
    
    private static final Map<String, Character> rusMultyTongMap = new HashMap<String, Character>(); 
    static {
    	rusMultyTongMap.put("Chsh", 'Щ');
    	rusMultyTongMap.put("chsh", 'щ');
    }
    
    private static final Map<String, Character> rusDifTongMap = new HashMap<String, Character>(); 
    static {
    	rusDifTongMap.put("Yo", 'Ё');
    	rusDifTongMap.put("yo", 'ё');
    	rusDifTongMap.put("Ye", 'Э');
    	rusDifTongMap.put("ye", 'э');
    	rusDifTongMap.put("Yi", 'Й');
    	rusDifTongMap.put("yi", 'й');
    	rusDifTongMap.put("Zh", 'Ж');
    	rusDifTongMap.put("Ch", 'Ч');
    	rusDifTongMap.put("Sh", 'Ш');
    	rusDifTongMap.put("Yu", 'Ю');
    	rusDifTongMap.put("Ya", 'Я');
    	rusDifTongMap.put("zh", 'ж');
    	rusDifTongMap.put("ch", 'ч');
    	rusDifTongMap.put("sh", 'ш');
    	rusDifTongMap.put("yu", 'ю');
    	rusDifTongMap.put("ya", 'я'); 	
    }
  
    private static final Map<String, Character> latRuMap = new HashMap<String, Character>();

    static {
        latRuMap.put("A", 'А');
        latRuMap.put("B", 'Б');
        latRuMap.put("V", 'В');
        latRuMap.put("W", 'В');
        latRuMap.put("G", 'Г');
        latRuMap.put("D", 'Д');
        latRuMap.put("E", 'Е');
        latRuMap.put("E", 'Ё');
        latRuMap.put("Z", 'З');
        latRuMap.put("I", 'И');
        latRuMap.put("K", 'К');
        latRuMap.put("L", 'Л');
        latRuMap.put("M", 'М');
        latRuMap.put("N", 'Н');
        latRuMap.put("O", 'О');
        latRuMap.put("P", 'П');
        latRuMap.put("R", 'Р');
        latRuMap.put("S", 'С');
        latRuMap.put("T", 'Т');
        latRuMap.put("U", 'у');
        latRuMap.put("F", 'Ф');
        latRuMap.put("H", 'Х');
        latRuMap.put("C", 'Ц');
        latRuMap.put("Y", 'Ы');
        latRuMap.put("'", 'Ь');
        latRuMap.put("a", 'а');
        latRuMap.put("b", 'б');
        latRuMap.put("v", 'в');
        latRuMap.put("w", 'В');
        latRuMap.put("g", 'г');
        latRuMap.put("d", 'д');
        latRuMap.put("e", 'е');
        latRuMap.put("z", 'з');
        latRuMap.put("i", 'и');
        latRuMap.put("k", 'к');
        latRuMap.put("l", 'л');
        latRuMap.put("m", 'м');
        latRuMap.put("n", 'н');
        latRuMap.put("o", 'о');
        latRuMap.put("p", 'п');
        latRuMap.put("r", 'р');
        latRuMap.put("s", 'с');
        latRuMap.put("t", 'т');
        latRuMap.put("u", 'у');
        latRuMap.put("f", 'ф');
        latRuMap.put("h", 'х');
        latRuMap.put("c", 'ц');
        latRuMap.put("y", 'ы');
        latRuMap.put("'", 'ь');
    }
    
    public static String lat_ru(String string) {
        StringBuilder transliteratedString = new StringBuilder();
        String replString = string;
        for(String diftong: rusMultyTongMap.keySet()) {
        	replString = replString.replaceAll(diftong, String.valueOf(rusMultyTongMap.get(diftong)));
        }
        
        for(String diftong: rusDifTongMap.keySet()) {
        	replString = replString.replaceAll(diftong, String.valueOf(rusDifTongMap.get(diftong)));
        }
        
        for (int i = 0; i < replString.length(); i++) {
            Character ch = replString.charAt(i);
            Character charFromMap = latRuMap.get(String.valueOf(ch));
            if (charFromMap == null) {
                transliteratedString.append(ch);
            } else {
                transliteratedString.append(charFromMap);
            }
        }
        return transliteratedString.toString();
    }
    
   
    
    
    public static void main(String[] argv) throws IOException {
    	String rus = "Проба пера! - Test off pensil - Это я юзаю русский текст - Щас усё при счастье будет!";
    	String lat = "Don't worry, Yozhichshka Luchc'o!";
    	
    	System.out.println(rus);
    	String l = ru_lat(rus);
    	System.out.println(l);
    	String r = lat_ru(l);
    	System.out.println(r);
    	
    	System.out.println("\n");
    	
    	System.out.println(lat);
    	String rl = lat_ru(lat);
    	System.out.println(rl);
    	String lr = ru_lat(rl);
    	System.out.println(lr);
   	
    	RandomAccessFile raf = new RandomAccessFile("testList.txt", "rw");
    	raf.writeUTF(rus);
    	raf.seek(2);
    	String b; //raf.readUTF();
    	while ((b = raf.readLine()) != null) {
    		System.out.println(b);
    		System.out.println(lat_ru(b));
    	}
    }
 }
