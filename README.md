# README #


### Quick summary ###

** Multiuser onLine game TIC-TAC toe with multilanguage support. **

** Mногопользовательская браузерная игра «Крестики-нолики». **

The game is playing under classical rule, see [«TIC-TAC toe»](https://en.wikipedia.org/wiki/Tic-tac-toe).

Server side is implemented on Java and Web-client is implemented by using MTML5, Web-socket, jQuery, CSS3, AJAX, JSON.

After registration user can view list other users that connecting to the game. In this list user see also a users state like "redy to play, in play with other gamer, invited by onother gamer".  To begin a play with free gamer it's need to invite him to play. The opponent can opportunity to agree or refuse invitation and only after opponent answer to first gamer obout his agree the game is beginnig.When the game is finished both gamers received   invitations to repeat game once more. As mentioning above any gamer from the pair can opportunity to agree or refuse to repeating game. After match beatween two gamer is finished both gamers are going on free to play with any onLine gamer.

1.      Игра удовлетворяет классическим правилам игры [«Крестики-нолики»](http://ru.wikipedia.org/wiki/%D0%9A%D1%80%D0%B5%D1%81%D1%82%D0%B8%D0%BA%D0%B8- %D0%BD%D0%BE%D0%BB%D0%B8%D0%BA%D0%B8).
2.	Реализована на языке программирования Java с использованием jQuery, CSS3, AJAX, JSON.
3.	Реализована с применением MVC-паттерна.
4.	Использовано объектно-ориентированное программирование.
5.	Многоязычный интерфейс: русский и английский.
6.	При первом входе на сайт игры, пользователю необходимо ввести имя и пароль
    в специальном окне, либо зарегистрироваться.
7.	Введя имя и пароль, пользователь видит список других игроков, подключенных к игре.
8.	Пользователь выбирает любого игрока и приглашает его в игру.
9.	Если Вас выбрали в качестве оппонента, Вы получаете приглашение в игру.
10.	Игра начинается после подтверждения приглашения.
11.	Первый ход выполняется приглашаемой стороной.
12.	После окончания игры предлагается повторить игру.
13.	В случае отказа, игрок может выбрать другого игрока.
15.	Используется возможности HTML 5. Обновление экрана браузера основано 
    на использование технологии WebSocket.
16.	Тестирование игры произведено на последних версиях браузеров Chrom & FireFox.



###  Version 1.0 ###


### Build project. Сборка проекта ###
Build of project make by using MAVEN: 

** mvn install

The resulting jar-file is lying in folder ** "target" ** with name ** "TicTacToe-1.0-SNAPSHOT.jar"** 

Сборка осуществляется под управлением maven, как обычно, командой:

** mvn install

вновь собранный файл серверного приложения находится в папке "target"
под именем ** TicTacToe-1.0-SNAPSHOT.jar **

### Run project (Server side). Запуск проекта ###
A Server side of project run under management of **Netty** Web-container. 
Before running server side it's need copy ** target/TicTacToe-1.0-SNAPSHOT.jar ** to root folder with name, for example, ** TicTacToe.jar **. It's need also that there are ** gson-2.3.1.jar, netty-3.3.1.Final.jar ** in root folder. 

For running server it's enough tu put next command:

**java -jar TicTacToe.jar **

As result of successfully running the server answer next message on screen:

** TicTacToe Server: Listening on port 9000 **


Для запуска серверного приложения желательно скопировать собранный проект из папки "target"
в текущий каталог, скажем, с именем ** "TicTacToe.jar"

Теперь осталось убедиться, что в текущем каталоге также находятся библиотеки:

**- gson-2.3.1 - это библиотек парсинга текстов JSON

**- netty-3.3.1.Final - это сам контейнер, в котором будет запускаться серверное приложение

Теперь, для запуска сервера осталось в командном процессоре дать команду:
**java -jar TicTacToe.jar **

В результате сервер ответит сообщением:

** TicTacToe Server: Listening on port 9000 **
------------------------------
Для начала игры любой желающий сыграть должен в браузере открыть страницу 
** tictactoe.html **,
которая находится в папке "WebContent".
Там же должны находиться папки "css", "img", "js", "languages", "db"  
с сопутствующими вспомогательными скриптами, картинками, стилями и словарями различных языков.

### Настройка приложения ###

Сама страница "tictactoe.html" обращается к локальному серверу по локальному адресу:
** ws://localhost:9000/websocket**
Соответственно, если пользователь желает поиграть, будучи не на том же компьютере,
на котором запущен сервер, ему надо на своей странице, точнее в файле 
** WebContent/js/webSocket.js **
 поменять локальный адрес на сетевой, 
например, когда данное приложение запущено и доступно по внешнему адресу интернета
 
### Сопроводительная документация ###
Функциональные возможности программы, описание пользовательского интерфейса 
и заложенной в проекте игровой логики можно посмотреть 
в прилагаемом здесь же подробном документе