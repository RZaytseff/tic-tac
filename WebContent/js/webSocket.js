// Constants - WebSocket URL
var WEBSOCKET_URL = "ws://localhost:9000/websocket";


// Constants - Status Updates
var COLOR_GREEN =  '"#009933"';
var COLOR_ORANGE = '"#FF9900"';
var COLOR_RED =    '"#FF0000"';
var COLOR_GROW =   "#777777";

// user interface constants 
//var WEBSOCKET_CLOSED_STATUS = "Соединение с сервером разорвано.";
//var STRATEGIZING_STATUS = "Ход противника";
//var WAITING_STATUS = "Жду противника";
//var YOUR_TURN_STATUS = "Мой ход!";
//var YOU_WIN_STATUS  = "ПОБЕДА!";
//var YOU_LOSE_STATUS = "ПОРАЖЕНИЕ...";
//var TIED_STATUS = "НИЧЬЯ";
//var GAMER_IS_WINNER = " - победитель!";
//
//var INVITED = "Получено приглашение";
//var INVITATION_CONFIRMED = "Приглашение принято!";
//var INVITATION_CONFIRMED_CONTINUE = "Игра начнётся через 2 секунды";
//var REFUSE = "Отказ!";
//var REFUSE_COMMENT = "Приглашение сыграть не принято";
//var SEND_INVITATION = "Отправка приглашения:";
//var SEND_INVITATION_COMMENT = "Пригласить для игры ";
//var YES_LETS_GAME = "Да, сыграем!";
//var AGAIN_BORING = "Опять скучать?!";
//var LETS_INVITE_ANYBODY = "Может пригласить ещё кого-нибудь сыграть ?!...";
//var PLAY_BEGIN = "Играем!";
//var INVITATION_CONFIRMED = "Приглашение принято!";
//var INVITATION_CONFIRMED_CONTINUE = "Игра начнётся через 2 секунды";
//var REFUSE = "Отказ!";
//var REFUSE_COMMENT = "Приглашение сыграть не принято";
//var SEND_INVITATION = "Отправка приглашения:";
//var SEND_INVITATION_COMMENT = "Пригласить для игры ";
//var YES_LETS_GAME = "Да, сыграем!";
//var AGAIN_BORING = "Опять скучать?!";
//var LETS_INVITE_ANYBODY = "Может пригласить ещё кого-нибудь сыграть ?!...";
//var PLAY_BEGIN = "Играем!";
//
//var INVITED_COMMENT = "Вас приглашает сыграть ";
//var SO_LETS_GAMING = "Итак, начнём игру с ";
//var LABEL_SCORE "Счёт: ";
//var ONCE_MORE "Ещё партейку";
//
//var WEBSOCKET_CLOSED_STATUS_EN = "Connection has been closed.";
//
//var STRATEGIZING_STATUS_EN = "Your opponent's turn.";
//var WAITING_STATUS_EN = "Waiting for an opponent.";
//var YOUR_TURN_STATUS_EN = "It's your turn!";
//var YOU_WIN_STATUS_EN  = "WINNER!";
//var YOU_LOSE_STATUS_EN = "LOOSER...";
//var TIED_STATUS_EN = "The game is tied.";
//var GAMER_IS_WINNER_EN = " is the winner!"
//var NO_LATER;

// Constants - Game over message types
//var GAME_OVER_YOU_WIN = "YOU_WIN";
//var GAME_OVER_TIED = "TIED";

// Constants - Game
var PLAYER_O = "O";
var PLAYER_X = "X";

// Constants - Incoming message types
var MESSAGE_LOGIN = "login";
var MESSAGE_REGISTRATION = "registration";
var MESSAGE_GAMER_LIST = "gamerList";

var MESSAGE_GAMER_PRESENCE_LIST = "presenceList";
var MESSAGE_GAMER_PRESENCE_STATE = "gamerPresenceState";

var GAMER_STATE_READY = 'ready';
var GAMER_STATE_GAMING = 'gaming';
var GAMER_STATE_BUSY =  'busy';
var GAMER_STATE_OFF_LINE = 'offLine';
var GAMER_STATE_UNDEFINED = 'undefined';

var GAMER_STATE_COLOR_READY = 'green';
var GAMER_STATE_COLOR_GAMING = 'red';
var GAMER_STATE_COLOR_BUSY =  'orange';
var GAMER_STATE_COLOR_OFF_LINE = 'gray';
var GAMER_STATE_COLOR_UNDEFINED = 'gray';

var GET_GAMER_PESENCE_STATE = "getPresenceState";
var GET_GAMER_STATE = "getState";
var GET_GAME_SCORE =  "getScore";
var GET_GAME_STATUS = "getStatus";

var MESSAGE_GAMER_STATE = "gamerState";

var MESSAGE_GAME_SCORE =  "gameScore";
var MESSAGE_GAME = "game";
var MESSAGE_GAME_STATUS = "gameStatus";
var GAME_SCORE_FEFAULT = "--:--";

var GAME_STATUS_READY = "ready";
var GAME_STATUS_IN_GAME = "gaming";
var GAME_STATUS_INVITE = "invite";
var GAME_STATUS_INVITED = "invited";
var GAME_STATUS_CONFIRM = "confirm";
var GAME_STATUS_CONFIRMED = "confirmed";
var GAME_STATUS_REFUSE = "refuse";
var GAME_STATUS_REFUSED = "refused";
var GAME_STATUS_OFF_LINE = "offLine";
var GAME_STATUS_UNDEFINED = "undefined";

var GAME_STATUS_SIGN_READY = " ";
var GAME_STATUS_SIGN_IN_GAME = "↔";
var GAME_STATUS_SIGN_INVITE = "←";
var GAME_STATUS_SIGN_INVITED = "→";
var GAME_STATUS_SIGN_CONFIRM = "√";
var GAME_STATUS_SIGN_REFUSE = "♯";
var GAME_STATUS_SIGN_REFUSED = "ᴓ";
var GAME_STATUS_SIGN_OFF_LINE = " ";
var GAME_STATUS_SIGN_UNDEFINED = " ";

var MESSAGE_GAMER_INVITATION_LIST = "invitationList";

var MESSAGE_GAME_INVITATION = "invitation";
var MESSAGE_GAME_INVITE = "invite";
var MESSAGE_GAME_CONFIRM_INVITE = "confirm";
var MESSAGE_GAME_REFUSE_INVITE = "refuse";

var MESSAGE_HANDSHAKE = "handshake";
var MESSAGE_OPPONENT_UPDATE = "response";
var MESSAGE_TURN_INDICATOR = "turn";
var MESSAGE_GAME_OVER = "game_over";

// Constants - Message turn indicator types
var MESSAGE_TURN_INDICATOR_YOUR_TURN = "YOUR_TURN";
var MESSAGE_TURN_INDICATOR_WAITING = "WAITING";

// Constants - Game over message types
var MESSAGE_GAME_OVER_YOU_WIN = "YOU_WIN";
var MESSAGE_GAME_OVER_TIED = "TIED";

// Constants - WebSocket URL
var WEBSOCKET_URL = "ws://localhost:9000/websocket";

var GET_FILE = "file";
var GET_DICTIONARY = "dictionary";
var GET_DICTIONARY_LIST = "stringList";

// Variables
var nic;
var name;

var player;
var opponent;
var opponentNic;
var gameId;
var yourTurn = false;

// WebSocket connection
var ws;
var gamers = []; 
var game;
var presenceList = [];

var confirm = true;

var webSocket = (function($) { 
	var init;
		
	init = function() {  
	    
	  if (typeof MozWebSocket != "undefined") { // (window.MozWebSocket)
	     ws = new MozWebSocket(WEBSOCKET_URL);
	  } else if (window.WebSocket) {
	     ws = new WebSocket(WEBSOCKET_URL);
	  } else {
	    swal({
	    		type: "error",
	    		title: translate('CONFUSE'),   
	    		text:  translate('WEB_SOCKET_NOT_SUPPORTED'),   
	    		timer: 4000,   
	    		showConfirmButton: false
	    	});
	  }	
	  
	  getAllGamerNic();
	  
	//  initGameTable(this.id);
	  
	    ws.onopen = function(event) {
	    	
	    	getDictionaryList(LANGUAGE);  // язык по умолчанию - русский
	    	
	    	$('#status').text(translate('WAITING_STATUS')); 
	    };
	    
	    // Process turn message ("push") from the server.
	    ws.onmessage = function(event) {
	            var message = jQuery.parseJSON(event.data);

	            /* The initialization of select gamer nic options */
		        if (message.type === MESSAGE_GAMER_LIST) { 
		                gamers = message.gamerList;    		
		                var options = $('#nicList')[0];
		                for (var i = 0; i < gamers.length; i++) { 
		                    var gamer = gamers[i]; 
		                    options[i] = new Option(gamer.name, gamer.nic);
		                }
		                return;
		        }
		        
		        // перевод страницы на требуемый язык
		        if(message.type === GET_DICTIONARY_LIST) {
		        	TRANSLATE = new Map();
		        	//записываем словарь в глобальную переменную
		        	for(var i = 0; i < message.list.length; i++) 
		        		TRANSLATE.set(message.list[i].split("=")[0].trim().toUpperCase(), message.list[i].split("=")[1].trim())
		        			    		
					$('body').find("[translate]").each(function () { 
						var tag = $(this)[0].tagName.toLowerCase();
						var tran = translate($(this).attr('translate').trim().toUpperCase());
						
						switch (tag) {
							case "input":
								$(this).val(tran);
								if(!($(this).attr('inputPlaceholder') === undefined))
									$(this).attr('inputPlaceholder', translate($(this).attr('inputPlaceholder').trim()));
								break;
							case "div": 
								$(this).attr('title', tran); 
								break;
							default:		
								$(this).html(tran); 
								break;
						}
					});
 
		        }
		        
		        if (message.type === MESSAGE_GAMER_PRESENCE_LIST) { 				
						var table = '<table id = "gamerScoreTable" width="100%" border="0" cellspacing="0" cellpadding="0"> </table>';
						$('#gamerScoreListDiv').html(table);
						var $table = $('#gamerScoreTable');
						
		                presenceList = message.presenceList; 
		                for (var i = 0; i < presenceList.length; i++) { 
		                    var gamer = presenceList[i]; 
							$table.append(newTr(gamer.nic)); 
							setGamerState(gamer.nic, gamer.state,  GAMER_STATE_OFF_LINE);
							setGameScore (gamer.nic, gamer.score,  GAME_SCORE_FEFAULT);
							setGameStatus(gamer.nic, gamer.status, GAME_STATUS_SIGN_UNDEFINED);
		                }

		                
//		                $('#gamerScoreTable td[title]').click(function(){invite(this.innerHTML)} );
		                
		                toolTip.init();
		                
		                return;
		        }
		        	
		        if (message.type === MESSAGE_GAMER_PRESENCE_STATE) { 
					var gamerNicQuantity = $('#gamerScoreTable #' + message.nic + '_gamer').length;
					var isNewGamer = (gamerNicQuantity == 0);
					// такого игрока ещё нет на экране
					if(isNewGamer) {					
						$('#gamerScoreTable').append(newTr(message.nic)); 
						if(! (message.state === undefined))  setGamerState(message.nic, message.state,  GAMER_STATE_OFF_LINE);
						if(! (message.score === undefined))  setGameScore (message.nic, message.score,  GAME_SCORE_FEFAULT);
						if(! (message.status === undefined)) setGameStatus(message.nic, message.status, GAME_STATUS_SIGN_UNDEFINED);
					} else {
						if(! (message.state === undefined))  setGamerState(message.nic, message.state);
						if(! (message.score === undefined))  setGameScore (message.nic, message.score);
						if(! (message.status === undefined)) setGameStatus(message.nic, message.status);
					}
//       				toolTip.init();
						
					return;
				}
		        
		        
		        // Получено приглашение сыграть
	            if (message.type === MESSAGE_GAME_INVITATION && message.status === GAME_STATUS_INVITED) {
	            	opponentNic = message.nic;
	            	setGamerState(message.nic, GAMER_STATE_BUSY);
	            	sendGamerState(GAMER_STATE_BUSY);
	            	setGameStatus(message.nic, GAME_STATUS_INVITED);
	                if(invited(message.nic)) initGameTable();
	                else {
	                	$('#gameDiv').hide();
	                	$('#gameButtomsDiv').hide();
	                };
	                return;
	            }
	            
		        // Приглашение сыграть ПРИНЯТО
	            if (message.type === MESSAGE_GAME_INVITATION && message.status === GAME_STATUS_CONFIRM) {
	            	opponentNic = message.nic;
	            	confirmInvitation(message.nic);
	            	swal({  title: translate('INVITATION_CONFIRMED'), 
	            			text:  translate('INVITATION_CONFIRMED_CONTINUE'), 
	            			type: "info",
	            			timer: 2000,   
	            			showConfirmButton: false
            			});
	            	sendInvitationStatus(message.nic, GAME_STATUS_CONFIRMED);
	                initGameTable();
	            	return;
	            }

		        // В приглашении сыграть ОTКАЗАНО
	            if (message.type === MESSAGE_GAME_INVITATION  && message.status === GAME_STATUS_REFUSE) {
	            	// разные значки для отказа приглашения для того, кто отказал и того, кто получил отказ
	            	refuseInvitation(message.nic, GAME_STATUS_REFUSE);
	            	closeGameModalPanel();

	            	// показ извещения об отказе 
	            	swal({  title: translate('REFUSE'), 
	            			text:  translate('REFUSE_COMMENT'),
	            			type: "error",
	            			timer: 2000,   
	            			showConfirmButton: false
	            			});
	            	
	            	return;
	            }

	            // Process the handshake response when the page is opened
	            if (message.type === MESSAGE_HANDSHAKE) {
	                gameId = message.gameId;
	                player = message.player;
	
	                if (player === PLAYER_X) {
	                        opponent = PLAYER_O; 
	                } else {
	                        opponent = PLAYER_X;   	 	 	
	                }
	            }
	            // Process your opponent's turn data.
	            if (message.type === MESSAGE_OPPONENT_UPDATE) {
	                // Show their turn info on the game board.
	                $("#" + message.gridId).addClass(message.opponent);
	                $("#" + message.gridId).html(message.opponent);
	                // Switch to your turn.
	                if (message.winner == true) {
	                        $('#status').text(message.opponent + ' ' + translate('GAMER_IS_WINNER'));
	                        var score = $('#gameScore').text();
	                        var resultText = opponentNic + ' ';
	                        resultText += translate('GAMER_IS_WINNER');
	                        continueGameConfirm(false, resultText, score);
	                        
//	                        $('#gameButtomsDiv').show();
	                        
	                } else if (message.tied == true) {
	                        $('#status').text(translate('TIED_STATUS'));  
	                        $('#gameButtomsDiv').show();
	                } else {
	                        yourTurn = true;
	                        $('#status').text(translate('YOUR_TURN_STATUS'));    	   	 			
	                }
	            }   	 	
	            /* The initial turn indicator from the server. Determines who starts
	                the game first. Both players wait until the server gives the OK
	                to start a game. */
	            if (message.type === MESSAGE_TURN_INDICATOR) {
	                    if (message.turn === MESSAGE_TURN_INDICATOR_YOUR_TURN) {
	                            yourTurn = true;
	                    $('#status').text(translate('YOUR_TURN_STATUS'));    	 			
	                } else if (message.turn === MESSAGE_TURN_INDICATOR_WAITING) {
	                            $('#status').text(translate('STRATEGIZING_STATUS'));    	 					    	
	                }
	            }
	
	            /* The server has determined you are the winner and sent you this message. */
	            if (message.type === MESSAGE_GAME_OVER) {
	            	 	var score = $('#gameScore').text();
	                    if (message.result === MESSAGE_GAME_OVER_YOU_WIN) {
	                            $('#status').text(translate('YOU_WIN_STATUS'));
	                            score = increaseWin(opponentNic);
	                            continueGameConfirm(true, translate('YOU_WIN_STATUS'), score);
	                    } else if (message.result === MESSAGE_GAME_OVER_TIED) {
	                            $('#status').text(TIED_STATUS);
	                            continueGameConfirm(null, translate('YOU_WIN_STATUS'), score);
	                    }
	                    
//	                    $('#gameButtomsDiv').show();
	                    
	            }	
	    }; 
	
	    ws.onclose = function(event) { 
	            $('#status').text(translate('WEBSOCKET_CLOSED_STATUS')); 
	            return true;
	    };		
	};
	return {init: init}; 
})(jQuery);




//Send current gamer NIC to the server.
function login(nic) { 
	var message = {type: MESSAGE_LOGIN, nic: nic};
	sendMessage(message);
	return true;
}

//Send current NIC and NAME to the server.
function registration(nic, name) {
	var message = {type: MESSAGE_REGISTRATION, nic: nic, name: name};
	sendMessage(message);
	return true;
}

//Get all gamers NIC from the server.
function getAllGamerNic() { 
	$("nic").click(function () {
		var message = {type: 'getInfo', operation: 'AllGamerNic'};
		sendMessage(message);
	});
}

//Get all gamers NIC from the server.
function getGamerPresenceList(nic) { 
//	$("nic").click(function () {
		var message = {type: MESSAGE_GAMER_PRESENCE_LIST, nic: nic};
		sendMessage(message);
		return true;
//	});
}

function getGamerPresenceState(nic) {
	sendMessage({type: GET_GAMER_PESENCE_STATE, nic: nic});
	return true;
} 

function getGamerState(nic) {
	sendMessage({type:  GET_GAMER_STATE, nic: nic});
	return true;
} 

function getGameScore(playerX, playerO) {
	sendMessage({type: GET_GAME_SCORE, gamerX: playerX, gamerO: playerO});
	return true;
} 

function getGameStatus(playerX, playerO) {
	sendMessage({type: GET_GAME_STATUS, gamerX: playerX, gamerO: playerO}); 
	return true;
} 

function getInvitationList(nic) {
	sendMessage({type: MESSAGE_GAMER_INVITATION_LIST, nic: nic});
}

function sendInvitationStatus(nic, status) {
	sendMessage({type: MESSAGE_GAME_INVITATION, status: status, nic: nic});
	return;
}

function inviting(nic) {
	setGamerState(nic, GAMER_STATE_BUSY);
	sendGamerState(nic,GAMER_STATE_BUSY);

	setGameStatus(nic, GAME_STATUS_INVITE);
	sendInvitationStatus(nic, MESSAGE_GAME_INVITE);
	return true;
}

function invite(nic) {
	swal({	
		title: translate('SEND_INVITATION'),  
		text:  translate('SEND_INVITATION_COMMENT') + " " + nic + " ?",   
		type: "warning", 
		timer: 4000, 
		animation:	true,
		showCancelButton: true, 
		cancelButtonText: translate('NO_LATER'), 
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: translate('YES_LETS_GAME'), 
		closeOnConfirm: false,
		closeOnCancel: false 
		}, 
		function(isConfirm){ 
			if(isConfirm === undefined || isConfirm == null || isConfirm == 'null')
				return;
			if(!isConfirm) {
            	swal({  
            		title: translate('AGAIN_BORING'), 
        			text:  translate('LETS_INVITE_ANYBODY'),
        			type: "error",
        			timer: 2000,   
        			showConfirmButton: false
    				});

			} else {	
            	swal({  
            		title:translate('PLAY_BEGIN'),
        			text: translate('SEND_INVITATION') + " " + nic + "!",
        			type: "success",
        			timer: 1000,   
        			showConfirmButton: false
    				});
				inviting(nic);
			}
			return isConfirm;
		});

	return true;
}

function newTr(nic) {
	var tr = '<tr id="'  + nic + '_gamer" width="100%">';
	tr += '<td id="' + nic + '_presence" width="9%" ><center><h3> <strong> ● </strong></h3></center></td>';
	tr += '<td id="' + nic + '_nic" width="50%">' +  nic + '</td>' ;
	tr += '<td id="' + nic + '_score" width="29%"><center>--:--</center></td>';
	tr += '<td id="' + nic + '_status" width="12%"><center><h3> <strong> </strong></h3></center></td>';
	tr += ' </tr>';
	return tr;
}


function setGamerState(nic, state, defaultState) {
	var newColor = GAMER_STATE_COLOR_UNDEFINED;
	var newColorDefault = GAMER_STATE_COLOR_UNDEFINED;
	switch (state) { 
		case GAMER_STATE_READY:    newColor =  newColorDefault = GAMER_STATE_COLOR_READY;     break;
		case GAMER_STATE_GAMING:   newColor =  newColorDefault = GAMER_STATE_COLOR_GAMING;    break;
		case GAMER_STATE_BUSY:     newColor =  newColorDefault = GAMER_STATE_COLOR_BUSY;      break;
		case GAMER_STATE_OFF_LINE: newColor =  newColorDefault = GAMER_STATE_COLOR_OFF_LINE;  break;
		case undefined:            newColor =  newColorDefault = GAMER_STATE_COLOR_UNDEFINED; break;
	}
	if(GAMER_STATE_READY === state) { 
		$('#gamerScoreTable #' + nic + '_gamer').css('color', newColor).attr('onClick', 'invite(' + '"' + nic + '")')
								.addClass("jQtooltip mini").attr("title",translate('INVITE'));
	} else {
		if(!(state  === undefined || state == '' || state == 'null' || state == 'undefined')) {
			$('#gamerScoreTable #' + nic + '_gamer').css('color', newColor);
		} else {
			if(defaultState=== undefined || defaultState == '' || defaultState == 'null' || defaultState == 'undefined') {
				$('#gamerScoreTable #' + nic + '_gamer').css('color', newColorDefault);
			}	
		}
		$('#gamerScoreTable #' + nic + '_gamer').removeAttr('class')
								.removeAttr('title').removeAttr('translate').removeAttr('onClick');
	}
}

function setGameScore(nic, score, scoreDefault) {
	if(!(score  === undefined || score == '' || score == 'null' || score == 'undefined')) 
		$('#gamerScoreTable #' + nic + '_score center').text(score);
	else if(!(scoreDefault  === undefined || scoreDefault == '' || scoreDefault == 'null' || scoreDefault == 'undefined')) 
		$('#gamerScoreTable #' + nic + '_score center').text(scoreDefault);
}

function setGameStatus(nic, status, statusDefault) {
	var statusSign = GAME_STATUS_SIGN_UNDEFINED; // √↔ↄⱷ♯ᴓ
	var statusDefaultSign = GAME_STATUS_SIGN_UNDEFINED;
	
	switch (status) { 
		case GAME_STATUS_READY:    statusSign = statusDefaultSign = GAME_STATUS_SIGN_READY; break;
		case GAME_STATUS_IN_GAME:  statusSign = statusDefaultSign = GAME_STATUS_SIGN_IN_GAME; break;
		case GAME_STATUS_INVITE:   statusSign = statusDefaultSign = GAME_STATUS_SIGN_INVITE; break;
		case GAME_STATUS_INVITED:  statusSign = statusDefaultSign = GAME_STATUS_SIGN_INVITED; break;
		case GAME_STATUS_CONFIRM:  statusSign = statusDefaultSign = GAME_STATUS_SIGN_CONFIRM; break;
		case GAME_STATUS_REFUSE:   statusSign = statusDefaultSign = GAME_STATUS_SIGN_REFUSE; break;
		case GAME_STATUS_REFUSED:  statusSign = statusDefaultSign = GAME_STATUS_SIGN_REFUSED; break;
		case GAME_STATUS_OFF_LINE: statusSign = statusDefaultSign = GAME_STATUS_SIGN_OFF_LINE; break;
		case undefined:            statusSign = statusDefaultSign = GAME_STATUS_SIGN_UNDEFINED; break;
	}
	
	if(!(status  === undefined || status == '' || status == 'null' || status == 'undefined')) 
		$('#gamerScoreTable #' + nic + '_status center h3 strong').text(statusSign);
	else if(!(status  === undefined || status == '' || status == 'null' || status == 'undefined')) 
		$('#gamerScoreTable #' + nic + '_status center h3 strong').text(statusDefaultSign);
}

function invited(nic) {
	swal({	
		title: translate('INVITED'),   
		text:  translate('INVITED_COMMENT') + " " + nic + "!",   
		type: "warning", 
		timer: 15000, 
		animation:	true,
		showCancelButton: true, 
		cancelButtonText: translate('NO_LATER'),
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: translate('YES_LETS_GAME'),   
		closeOnConfirm: false,
		closeOnCancel: false 
		}, 
		function(isConfirm){ 
			if(isConfirm === undefined || isConfirm == null || isConfirm == 'null')
				return;
			if(isConfirm) {
            	swal({  
            		title:translate('YES_LETS_GAME'),
        			text: translate('SO_LETS_GAMING') + " " + nic + "!",
        			type: "success",
        			timer: 1000,   
        			showConfirmButton: false
    				}
            	);
				confirmInvitation(nic);
				sendInvitationStatus(nic, GAME_STATUS_CONFIRM);
				return isConfirm;
			} else {	
            	swal({  
            		title: translate('AGAIN_BORING'),
        			text:  translate('LETS_INVITE_ANYBODY'),
        			type: "error",
        			timer: 2000,   
        			showConfirmButton: false
    				});
            	
            	$('#gameDiv').hide();

				refuseInvitation(nic);
				sendInvitationStatus(nic, GAME_STATUS_REFUSE);
				return !isConfirm;
			}
		});
	
	return true;
}

function confirmInvitation(nic) {
	opponentNic = nic;
	setGamerState(nic, GAMER_STATE_GAMING);
	sendGamerState(nic, GAMER_STATE_GAMING);
	setGameStatus(nic, GAME_STATUS_CONFIRM);
	
	// show & init game panel on screen
	initGameTable();
	return true;
}

function refuseInvitation(nic, gameStatus) {
	setGamerState(nic, GAMER_STATE_READY);
	sendGamerState(nic, GAMER_STATE_READY);
	// разные значки для отказавшего и получившего отказ игроков
	if(gameStatus === undefined)
		setGameStatus(nic, GAME_STATUS_REFUSE);
	else 
		setGameStatus(nic, GAME_STATUS_REFUSED);
	
	// close game panel on screen 
	closeGameModalPanel(true);

	return true;
}

function sendGamerState(nic, state) {
	sendMessage({type: MESSAGE_GAMER_STATE, nic:nic, state: state});
}

function increaseWin(nic) {
	var presenceScore = $('#gamerScoreTable #' + nic + '_score center');
	var gameScore = $('#gameScore');
	var scoreSet = gameScore.text();
	if('--:--' === scoreSet) {
		score = '1 : 0';
	} else {	
		var scoreWin;
		if('--' === scoreSet.split(':')[0]) 
			scoreWin = 0;
		else
			scoreWin = parseInt(scoreSet.split(':')[0].trim());
		
		var scoreLose;
		if('--' === scoreSet.split(':')[0]) 
			scoreLose = 0;
		else 
			scoreLose = parseInt(scoreSet.split(":")[1].trim());
		
		scoreWin++;
	
		score = '' + scoreWin + ':' + scoreLose;
	}
	// set score to game Table
	gameScore.text(score);
	// set score to presence table
	presenceScore.text(score);

	return score;
}

function increaseLose(nic) {
	var presenceScore = $('#gamerScoreTable #' + nic + '_score center');
	var gameScore = $('#gameScore');
	var scoreSet = gameScore.text();

	if('--:--' === scoreSet) {
		score = '0 : 1';
	} else {
		var scoreWin;
		if('--' === scoreSet.split(':')[0].trim()) 
			scoreWin = 0;
		else
			scoreWin = parseInt(scoreSet.split(':')[0].trim());
		
		var scoreLose;
		if('--' === scoreSet.split(':')[0].trim()) 
			scoreLose = 0;
		else 
			scoreLose = parseInt(scoreSet.split(":")[1].trim());
		
		scoreLose++;
		score = '' + scoreWin + ' : ' + scoreLose;
	}

	// set score to presence table
	presenceScore.text(score);
	// set score to game Table
	$('#gameScore').text(score);

	return score;
}

//Send your turn information to the server.
function sendGameMessage(id) {
	var message = {type: MESSAGE_GAME, gameId: gameId, player: player, gridId:id};
	sendMessage(message);
	return true;
}

function sendMessage(message) {
	var encoded = $.toJSON(message);
	ws.send(encoded);
	return true;
}

function getDictionaryList(countryCode) {
	var fileName = 'languages/' + countryCode + '.lang'; 
	sendMessage({type: GET_DICTIONARY_LIST, url: fileName});
}


function continueGameConfirm(gameWon, gameResultText, score) {
	var icon = 'img/thumbs-down.jpg';
	if(gameWon) icon = 'img/thumbs-up.jpg';
	if(gameWon === undefined) icon = 'img/info.jpg';
	
	swal({	
		title: gameResultText,   
		text: translate('LABEL_SCORE') + " " + score,   
		imageUrl: icon, 
		timer: 2000, 
		animation:	true,
		showCancelButton: true, 
		cancelButtonText: translate('NO_LATER'),
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: translate('ONCE_MORE'),   
		closeOnConfirm: true,
		closeOnCancel: true 
		}, 
		function(isConfirm){ 
			if(isConfirm === undefined || isConfirm == null || isConfirm == 'null')
				return;
			gameOnceMore(isConfirm);
			return isConfirm;
		}
	);
	
}

function gameOnceMore(mode) {
	if(mode) {
		initGameTable(); 
		inviting(opponentNic);
		setGamerState(nic, GAMER_STATE_BUSY);
	} else {
		$('#gameDiv').hide();
		setGamerState(nic, GAMER_STATE_READY);
		setGameStatus(nic, GAME_STATUS_READY);
		sendGamerState(nic, GAMER_STATE_READY);
	}	
}

function initGameTable() {	
	// copy score to game Table from presence table
	var scoreInPresenceTable = $('#' + opponentNic + '_score center').text();
	if(scoreInPresenceTable == "--:--" || scoreInPresenceTable == "-- : --") 
		score = " 0:0 ";
	else 
		score = scoreInPresenceTable;
	
//	$('#gameDiv').show();
//	$('#gameButtomsDiv').hide();

	initGameModalPanel();
	
	$('#gameScore').text(score);
	
	for(var i = 1; i < 10; i++) $('#grid_' + i).text(" "); //.removeAttr('class'); //. removeAttr('html');
	
	// bind to .grid class 
	$(".grid").click(function () { 
            // Only process clicks if it's your turn.
		if (yourTurn == true) { 
	    // Stop processing clicks and invoke sendMessage(). 
	        yourTurn = false;
	        sendGameMessage(this.id);
		    // Add the X or O to the game board and update status.
	      $("#" + this.id).addClass(player);
	      $("#" + this.id).html(player);	    	  
	      $('#status').text(translate('STRATEGIZING_STATUS'));    	 					      
		}
    });	
};

function initGameModalPanel() { // перемещаемое модальное окно
	$('#overlayGame').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
    	 	function(){ // пoсле выпoлнения предыдущей aнимaции
    			$('#gameDiv').draggable() 
    				.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
    				.animate({opacity: 1, top: '50%'}, 300); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
    	})
    	;
    	
    	/* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    	$('#gamePanelClose').click( function(){ // лoвим клик пo крестику или пoдлoжке
    		closeGameModalPanel(true);
			sendInvitationStatus(opponentNic, GAME_STATUS_REFUSE);
    	});
};

function closeGameModalPanel(mode) { // перемещаемое модальное окно
    		$('#gameDiv')
    			.animate({opacity: 0, top: '45%'}, 300,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
    				function(){ // пoсле aнимaции
    					$(this).css('display', 'none'); // делaем ему display: none;
    					$('#overlayGame').fadeOut(400); // скрывaем пoдлoжку
    				}
    			);
    		
    		gameOnceMore(false);
			if(!mode) refuseInvitation(opponentNic, GAME_STATUS_REFUSE);
 };

function translate(key) {
	if (typeof(TRANSLATE.get(key)) != 'undefined') 
		return TRANSLATE.get(key); 
	return key; 
}
