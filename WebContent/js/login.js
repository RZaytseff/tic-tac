// @Author RZaytsev

//var ENTER = "Войти";
//var REGISTRATION = "Регистрация";
//var GAMER_EXIST = "Такой игрок уже зарегистрирован!";
//var GAMER_NOT_EXIST = "Такой игрок ещё не зарегистрирован!";

var gamerNameVisible = false;

function checkLogin(value) { 
	if(value == translate('ENTER')) {
		var inputNic =  document.getElementById('gamerNic').value;
		initSlidePanel(jQuery('#spa'));
		
		login(inputNic);
		toggleGamePage(inputNic);
		getGamerPresenceList(inputNic);
		expandPanel.initModule(jQuery('#spa'));
		expandPanel.toggleSlider();
		
	} else {
		visibleGamerNameDiv();
	}
}

function doRegistration() { 
		var inputNic =  document.getElementById('gamerNic').value;
		var inputName =  document.getElementById('gamerName').value;
		initSlidePanel(jQuery('#spa'));
		registration(inputNic, inputName);
		toggleGamePage(inputNic);
		getGamerPresenceList(inputNic);
		expandPanel.initModule(jQuery('#spa'));
		expandPanel.toggleSlider();
}

function doContinue() {
	initSlidePanel(jQuery('#spa'));
	toggleGamePage(inputNic);
	getGamerPresenceList(inputNic);
	expandPanel.initModule(jQuery('#spa'));
	expandPanel.toggleSlider();
}

function toggleGamePage(inputNic) {
		document.getElementById('loginDiv').style.display='none';
		document.getElementById('loginDialog').style.display='none';
		return true;
}

function isUniqueGamerNic() {
	var inputNic =  document.getElementById('gamerNic').value; 
	for(var i = 0; i < gamers.length; i++) {
		if(inputNic == gamers[i].nic)  {
			document.getElementById('loginButton').value = translate('ENTER');
			document.getElementById('auth_comment_text').innerHTML = translate('GAMER_EXIST');
			return false;
		}
	}
	document.getElementById('loginButton').value = translate('REGISTRATION');
	document.getElementById('auth_comment_text').innerHTML = translate('GAMER_NOT_EXIST');
	return true;
}

function toggleUniqueGamerNic() {  
	var isNewGamer = isUniqueGamerNic(); 
	if(isNewGamer) visibleGamerNameDiv();
	else hideGamerNameDiv();
}

function visibleGamerNameDiv() {    
		document.getElementById('gamerNic').readonly = "true";
		document.getElementById('loginButtonDiv').style.display = 'none';
		document.getElementById('auth_name').style.display = 'initial';
		gamerNameVisible = false;
}

function hideGamerNameDiv() {
		document.getElementById('gamerNic').readonly = "false";
		document.getElementById('loginButtonDiv').style.display = 'initial';
		document.getElementById('auth_name').style.display = 'none';
		gamerNameVisible = true;					
}

